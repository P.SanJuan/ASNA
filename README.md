Introduction
---------------

The following repository contains all implementations mentioned in the paper:

P. San Juan, T. Virtanen, V. M. Garcia-Molla, A. M. VIdal
Efficient Parallel Implementation of Active-Set Newton Algorithm for Non-Negative Sparse Representations,
Computational and Mathematical Methods in Science and Engineering CMMSE 2017

The algorithm implemented is the Active Set Newton Algorithm (ASNA) by Tuomas Virtanen which is explained in the following papers:

T. Virtanen, J. F. Gemmeke, and B. Raj. Active-Set Newton
Algorithm for Overcomplete Non-Negative Representations of Audio,
IEEE Transactions on  Audio Speech and Language Processing,
volume 21 issue 11, 2013.

T. Virtanen, B. Raj, J. F. Gemmeke, and H. Van hamme, Active-set
Newton algorithm for non-negative sparse coding of audio, in proc.
International Conference on Acoustics, Speech and Signal
Processing, 2014, submitted for publication 

The papers and original sources can be found here: http://www.cs.tut.fi/~tuomasv/software.html


Repository structure
--------------------

The repository contains 4 implementations of the algorithm splited into 3 directories:

-**Original Matlab:** Contains the original MATLAB implementation of the algorithm by Tuomas Virtanen.
-**ImprovedMatlab:** Contains the improved MATLAB implementation by P. San Juan.
-**CImplementation:** Contains both the C implementation and the Parallel C Implementation by P. San Juan


Citation
-----------

When using the original matlab implementation you should cite the following papers:

T. Virtanen, J. F. Gemmeke, and B. Raj. Active-Set Newton
Algorithm for Overcomplete Non-Negative Representations of Audio,
IEEE Transactions on  Audio Speech and Language Processing,
volume 21 issue 11, 2013.

T. Virtanen, B. Raj, J. F. Gemmeke, and H. Van hamme, Active-set
Newton algorithm for non-negative sparse coding of audio, in proc.
International Conference on Acoustics, Speech and Signal
Processing, 2014, submitted for publication 

When using the Improved implementation or the C implementations you should cite the following papers:

T. Virtanen, J. F. Gemmeke, and B. Raj. Active-Set Newton
Algorithm for Overcomplete Non-Negative Representations of Audio,
IEEE Transactions on  Audio Speech and Language Processing,
volume 21 issue 11, 2013.

P. San Juan, T. Virtanen, V. M. Garcia-Molla, A. M. VIdal
Efficient Parallel Implementation of Active-Set Newton Algorithm for Non-Negative Sparse Representations,
Computational and Mathematical Methods in Science and Engineering CMMSE 2017




